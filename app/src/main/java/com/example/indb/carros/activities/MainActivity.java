package com.example.indb.carros.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.indb.carros.Constants;
import com.example.indb.carros.R;
import com.example.indb.carros.adapters.CarListAdapter;
import com.example.indb.carros.data.CarMock;
import com.example.indb.carros.entities.Car;
import com.example.indb.carros.listeners.OnListClickInteractionListener;

import java.util.List;

public class MainActivity extends AppCompatActivity  {

    private static class ViewHolder {
        RecyclerView recyclerCars;
    }

    ViewHolder mViewHolder = new ViewHolder();
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);


        CarMock carMock = new CarMock(this);
        List<Car> list = carMock.getList();

        //1 - Obter a RecyclerView
        this.mViewHolder.recyclerCars = (RecyclerView) this.findViewById(R.id.recycler_cars);

        mContext = this;

        OnListClickInteractionListener listener = new OnListClickInteractionListener() {
            @Override
            public void onClick(int id) {
                Bundle bundle = new Bundle();
                bundle.putInt(Constants.CARRO_ID, id);

                Intent intent = new Intent(mContext, DetailsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        };

        //Definir adapter
        CarListAdapter carListAdapter = new CarListAdapter(list, listener);
        this.mViewHolder.recyclerCars.setAdapter(carListAdapter);

        //Definir um layout
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        this.mViewHolder.recyclerCars.setLayoutManager(linearLayoutManager);

    }
}
