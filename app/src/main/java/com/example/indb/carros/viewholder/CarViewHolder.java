package com.example.indb.carros.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.indb.carros.R;
import com.example.indb.carros.entities.Car;
import com.example.indb.carros.listeners.OnListClickInteractionListener;

public class CarViewHolder extends RecyclerView.ViewHolder {

    private TextView mTextModel;
    private RelativeLayout relativeLayout;
    private TextView mShowDetails;

    public CarViewHolder(View itemView) {
        super(itemView);

        this.mTextModel = (TextView) itemView.findViewById(R.id.text_model);
        this.relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relative_layout);
        this.mShowDetails = (TextView) itemView.findViewById(R.id.showDetails);
    }

    public void bindData(final Car car, final OnListClickInteractionListener listener) {
        this.mTextModel.setText(car.model);
        this.relativeLayout.setBackground(car.picture);
        this.mShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(car.id);
            }
        });
    }
}
