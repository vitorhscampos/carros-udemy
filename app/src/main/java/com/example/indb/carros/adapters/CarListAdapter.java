package com.example.indb.carros.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.indb.carros.R;
import com.example.indb.carros.entities.Car;
import com.example.indb.carros.listeners.OnListClickInteractionListener;
import com.example.indb.carros.viewholder.CarViewHolder;

import java.util.List;

public class CarListAdapter extends RecyclerView.Adapter<CarViewHolder> {

    private List<Car> mListCars;
    private OnListClickInteractionListener mListener;

    public CarListAdapter(List<Car> cars, OnListClickInteractionListener listener){
        this.mListCars = cars;
        this.mListener = listener;
    }

    @Override
    public CarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View carView = inflater.inflate(R.layout.row_car_list, parent, false);
        return new CarViewHolder(carView);

    }

    @Override
    public void onBindViewHolder(CarViewHolder holder, int position) {
        Car car = this.mListCars.get(position);
        holder.bindData(car, this.mListener);
    }

    @Override
    public int getItemCount() {
        return this.mListCars.size();
    }
}
