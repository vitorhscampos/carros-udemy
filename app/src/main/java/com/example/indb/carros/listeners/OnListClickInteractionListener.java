package com.example.indb.carros.listeners;

public interface OnListClickInteractionListener {
    void onClick(int id);
}
