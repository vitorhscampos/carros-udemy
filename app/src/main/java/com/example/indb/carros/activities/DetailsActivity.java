package com.example.indb.carros.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.indb.carros.Constants;
import com.example.indb.carros.R;
import com.example.indb.carros.data.CarMock;
import com.example.indb.carros.entities.Car;

public class DetailsActivity extends AppCompatActivity {

    private static class ViewHolder {
        TextView txtModel;
        TextView txtHorsePower ;
        TextView txtPrice;
        TextView txtManufacturer;
        ImageView img;
    }

    private Car mCar;
    private CarMock mCarMock;
    private ViewHolder mViewHolder = new ViewHolder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);
        actionBar.setDisplayHomeAsUpEnabled(true);

        this.mCarMock = new CarMock(this);

        this.mViewHolder.txtModel = (TextView) this.findViewById(R.id.text_model);
        this.mViewHolder.txtHorsePower = (TextView) this.findViewById(R.id.text_horse_power);
        this.mViewHolder.txtPrice = (TextView) this.findViewById(R.id.text_price);
        this.mViewHolder.img = (ImageView) this.findViewById(R.id.car_pic);
        this.mViewHolder.txtManufacturer = (TextView) this.findViewById(R.id.text_manufacturer);

        this.getDataFromActivity();
        this.setData();
    }

    private void getDataFromActivity(){
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            this.mCar = this.mCarMock.getCar(extras.getInt(Constants.CARRO_ID));
        }
    }

    private void setData(){
        this.mViewHolder.txtModel.setText(mCar.model);
        this.mViewHolder.txtHorsePower.setText(String.valueOf(mCar.horsePower));
        this.mViewHolder.txtPrice.setText(String.valueOf(mCar.price));
        this.mViewHolder.img.setImageDrawable(mCar.picture);
        this.mViewHolder.txtManufacturer.setText(mCar.manufacturer);
    }
}
